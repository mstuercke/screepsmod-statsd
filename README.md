# screepsmod-statsd
This mod will fire all the stats of all users to your StatsD server.

[![pipeline status](https://gitlab.com/mstuercke/screepsmod-statsd/badges/master/pipeline.svg)](https://gitlab.com/mstuercke/screepsmod-statsd/commits/master)
[![npm version](https://badge.fury.io/js/screepsmod-statsd.svg)](https://npmjs.org/package/screepsmod-statsd)

## Installation
1. Go to your Screeps server directory and execute `npm install screepsmod-statsd`
2. Make sure, that the file `mods.json` contains the entry `"node_modules/screepsmod-statsd/index.js"`;
3. Restart the Screeps server

## Configuration
If you need to override the defaults, add the values you need to the `.screepsrc` file.

**Available Settings:** 
* `host`: The host to send stats to default: `localhost`
* `port`: The port to send stats to default: `8125`

**Example:**
```
[statsd]
host=localhost
port=8125
```

## Usage
* The GCL of all users will be added automatically to the stats
* To add values to your stats, you need to do it like this: 
    ```javascript
    if(!Memory.stats)
      Memory.stats = {};
    
    Memory.stats['tick'] = Game.time;
    Memory.stats['cpu.limit'] = Game.cpu.limit;
    ```

