const StatsD = require('node-statsd');
const ini = require('ini');
const fs = require('fs');

module.exports = function (config) {
  const db = config.common.storage.db,
      env = config.common.storage.env,
      client = new StatsD(readStatsdConfig());

  if (config.cronjobs) {
    config.cronjobs.updateGrafana = [1, async function () {
      const users = await db.users.find({});
      if(!users) {
        console.log('No users found to update stats for');
        return;
      }

      for (const user of users) {
        const userId = user._id;
        const memoryRaw = await env.get(env.keys.MEMORY + userId);

        const memory = JSON.parse(memoryRaw || "{}");

        client.gauge(user.usernameLower + '.gcl', user.gcl);
        if (!memory.stats) {
          continue;
        }

        report(client, memory.stats, user.usernameLower + '.');
      }
    }];
  }
};

function readStatsdConfig() {
  let screepsrc = {};
  try {
    screepsrc = ini.parse(fs.readFileSync('./.screepsrc', {encoding: 'utf8'}));
  } catch (e) {
  }
  return screepsrc.statsd || {};
}

function report(client, data, prefix) {
  if (prefix == null) {
    prefix = "";
  }
  return (() => {
    const result = [];
    for (let k in data) {
      const v = data[k];
      if (typeof v === 'object') {
        result.push(report(client, v, prefix + k + '.'));
      } else {
        result.push(client.gauge(prefix + k, v));
      }
    }
    return result;
  })();
}
