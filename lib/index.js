module.exports = function (config) {
  if (config.cronjobs) {
    require('./cronjobs')(config);
  }
};
